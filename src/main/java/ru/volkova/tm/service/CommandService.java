package ru.volkova.tm.service;

import ru.volkova.tm.api.ICommandRepository;
import ru.volkova.tm.api.ICommandService;
import ru.volkova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
