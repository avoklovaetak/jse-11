package ru.volkova.tm.api;

import ru.volkova.tm.model.Task;
import ru.volkova.tm.util.TerminalUtil;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

}
