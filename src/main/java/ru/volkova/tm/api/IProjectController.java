package ru.volkova.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectByName();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeProjectById();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

}
